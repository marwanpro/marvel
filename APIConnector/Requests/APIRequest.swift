//
//  APIRequest.swift
//  APIConnector
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation
import CommonCrypto
import Alamofire

class APIRequest {

    let apiKey = "1e00c2f70f6ef6b7dacb220ddfb9648d"
    let privateKey = "24c5104cee1e95bb2bb557f84ad53a77f7a5c50f"
    fileprivate var extraParam: [String: Any]?

    init(extraParameters: [String : Any]? = nil) {
        extraParam = extraParameters
    }

    var parameters: [String : Any] {
        let authParameters = authenticationParameters()
        guard var parameters = extraParam else {
            return authParameters
        }

        parameters.merge(dict: authParameters)
        return parameters
    }

    fileprivate func authenticationParameters() -> [String : Any] {
        let timestamp = String(Int(Date().timeIntervalSince1970))
               let hashData = stringToMD5(timestamp + privateKey + apiKey)
               let hashedKey = hashData.map { String(format: "%02X", $0) }.joined().lowercased()
               return [
                   "ts": timestamp,
                   "hash": hashedKey,
                   "apikey": apiKey
               ]
    }

    func request() -> DataRequest {
        return AF.request("https://gateway.marvel.com/v1/public/series", parameters:parameters).validate(contentType: ["application/json; charset=utf-8"])
    }

    func stringToMD5(_ string: String) -> Data {
        let dataLength = Int(CC_MD5_DIGEST_LENGTH)
        let stringData = string.data(using:.utf8)!
        var digestData = Data(count: dataLength)

        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            stringData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(stringData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
