//
//  IAPIConnector.swift
//  APIConnector
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation

public enum NetworkError: Error {
    case connection
    case serialisation
    case authorisation
}

public protocol IAPIConnector {

    func getSeries(offset:Int?, limit:Int?, result: @escaping ( Result<SeriesModel, NetworkError>) -> Void)

    func searchSeries(searchText: String, result: @escaping ( Result<SeriesModel, NetworkError>) -> Void)
    
}
