//
//  APIConnector.swift
//  APIConnectorTests
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation


public struct APIConnector: IAPIConnector {

    public static let shared: IAPIConnector = APIConnector()
    
    public func getSeries(offset:Int?, limit:Int?, result: @escaping ( Result<SeriesModel, NetworkError>) -> Void) {
        var extraParameters: [String : Any] = [:]
        if let limit = limit {
            extraParameters["limit"] = limit
        }

        if let offset = offset {
            extraParameters["offset"] = offset
        }

        apiRequest(withParameters: extraParameters, andResultHandler: result)
    }

    public func searchSeries(searchText: String, result: @escaping ( Result<SeriesModel, NetworkError>) -> Void) {
        apiRequest(withParameters: ["titleStartsWith": searchText], andResultHandler: result)
    }

    fileprivate func apiRequest(withParameters parameters: [String : Any], andResultHandler result: @escaping ( Result<SeriesModel, NetworkError>) -> Void) {
        let seriesRequest = SeriesRequest(extraParameters: parameters)
        seriesRequest.request()
            .validate(statusCode: 200..<300)
            .responseDecodable(of: SeriesModel.self) { response in
                switch response.result {
                case .success:
                    guard let seriesResponse = response.value else {
                        result(.failure(.serialisation))
                        return
                    }
                    result(.success(seriesResponse))
                case .failure:

                    result(.failure(.connection))
                }
        }
    }
}
