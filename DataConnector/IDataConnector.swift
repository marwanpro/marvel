//
//  IDataConnector.swift
//  DataConnector
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation

public enum DataConnectorError: Error {
    case connection
    case serialisation
    case authorisation
}

public protocol IDataConnector {

    func getSeries(offset:Int?, limit:Int?, result: @escaping ( Result<DataConnectorSeriesDataModel, DataConnectorError>) -> Void)

    func searchSeries(searchText: String, result: @escaping ( Result<DataConnectorSeriesDataModel, DataConnectorError>) -> Void)
}
