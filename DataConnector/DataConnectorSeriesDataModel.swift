//
//  DataConnectorSeriesDataModel.swift
//  DataConnector
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation


// MARK: - DataConnectorSeriesDataModel
public struct DataConnectorSeriesDataModel: Codable {
    public let offset, limit, total, count: Int?
    public var series: [SeriesDetails]?

    enum CodingKeys: String, CodingKey {
        case series = "results"
        case offset, limit, total, count
    }
}

// MARK: - Result
public struct SeriesDetails: Codable {
    public let id: Int?
    public let title: String?
    public let description: String?
    let resourceURI: String?
    let urls: [URLElement]?
    public let startYear, endYear: Int?
    public let rating: String?
    public let type, modified: String?
    public let thumbnail: Thumbnail?
    public let creators: Creators?
    public let characters: Characters?
    public let stories: Stories?
    public  let comics, events: Characters?
    public let next: Next?
    let previous: JSONNull?

    enum CodingKeys: String, CodingKey {
        case id, title
        case description
        case resourceURI, urls, startYear, endYear, rating, type, modified, thumbnail, creators, characters, stories, comics, events, next, previous
    }
}

// MARK: - Characters
public struct Characters: Codable {
    let available: Int?
    let collectionURI: String?
    let items: [Next]?
    let returned: Int?
}

// MARK: - Next
public struct Next: Codable {
    let resourceURI: String?
    let name: String?
}

// MARK: - Creators
public struct Creators: Codable {
    let available: Int?
    let collectionURI: String?
    let items: [CreatorsItem]?
    let returned: Int?
}

// MARK: - CreatorsItem
public struct CreatorsItem: Codable {
    let resourceURI: String?
    let name, role: String?
}

// MARK: - Stories
public struct Stories: Codable {
    let available: Int?
    let collectionURI: String?
    let items: [StoriesItem]?
    let returned: Int?
}

// MARK: - StoriesItem
public struct StoriesItem: Codable {
    let resourceURI: String?
    let name: String?
    let type: String?
}

// MARK: - Thumbnail
public struct Thumbnail: Codable {
    public let path: String?
    public let thumbnailExtension: String?

    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

// MARK: - URLElement
public struct URLElement: Codable {
    let type: String?
    let url: String?
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(0)
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
