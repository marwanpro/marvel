//
//  DataConnector.swift
//  DataConnector
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation
import APIConnector

public struct DataConnector: IDataConnector {

    public static let shared: IDataConnector = DataConnector()

    fileprivate func serialiseToDataConnector(_ model: DataInfo?) -> DataConnectorSeriesDataModel? {

        do {
            let jsonData = try JSONEncoder().encode(model)
            let dataConnectorModel = try JSONDecoder().decode(DataConnectorSeriesDataModel.self, from: jsonData)
            return dataConnectorModel
        } catch ( _) {
            return nil
        }
    }

    fileprivate func handleResponse(_ responseResult: Result<SeriesModel, NetworkError>, resultHandler: @escaping ( Result<DataConnectorSeriesDataModel, DataConnectorError>) -> Void) {
        switch(responseResult) {
        case .success(let series):
            if let dataConnectorModel = self.serialiseToDataConnector(series.dataInfo) {
                resultHandler(.success(dataConnectorModel))
            } else {
                resultHandler(.failure(.serialisation))
            }
        case .failure(let error):
            switch error {
            case .authorisation:
                resultHandler(.failure(.authorisation))
            case .connection:
                resultHandler(.failure(.connection))
            default:
                resultHandler(.failure(.serialisation))
            }
        }
    }

    public func getSeries(offset:Int?, limit:Int?, result: @escaping ( Result<DataConnectorSeriesDataModel, DataConnectorError>) -> Void) {
        APIConnector.shared.getSeries(offset: offset, limit: limit) { responseResult in
            self.handleResponse(responseResult, resultHandler: result)
        }
    }

    public func searchSeries(searchText: String, result: @escaping ( Result<DataConnectorSeriesDataModel, DataConnectorError>) -> Void) {
        APIConnector.shared.searchSeries(searchText: searchText) {  responseResult in
            self.handleResponse(responseResult, resultHandler: result)
        }
    }
}
