//
//  SeriesCollectionCellCollectionViewCell.swift
//  Marvel
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import UIKit

class SeriesCollectionViewCell: UICollectionViewCell {

    fileprivate let favoured = "⭐️"
    fileprivate let disfavoured = "✩"
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var endYear: UILabel!
    @IBOutlet weak var favouredIcon: UILabel!
    var seriesId: Int = 0
    let starIconTapObserver: ObservableValue<Int> = ObservableValue(0)

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        superview?.bringSubviewToFront(favouredIcon)
        backgroundColor = .gray
        setLayout()
        addTouchGestureToThumbnailIcon()
    }

    fileprivate func addTouchGestureToThumbnailIcon() {
        let touchGesture = UITapGestureRecognizer(target: self, action: #selector(onThumbnailTapped))
        touchGesture.numberOfTouchesRequired = 1
        favouredIcon.isUserInteractionEnabled = true
        favouredIcon.addGestureRecognizer(touchGesture)
    }

    @objc fileprivate func onThumbnailTapped() {
        starIconTapObserver.value = seriesId
    }


    func setFavoured() {
        favouredIcon.text = favoured
    }

    func setDisfavoured() {
        favouredIcon.text = disfavoured
        favouredIcon.textColor = .white
    }

    fileprivate func setLayout() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5
        self.layer.shadowRadius = 5
        self.layer.shadowOffset = CGSize(width: 5, height: 5)
    }

    func loadThumbnailImage(url: String) {
        DispatchQueue.global(qos: .background).async {
            if let imageUrl = URL(string: url), let imageData = try? Data(contentsOf: imageUrl) {
                DispatchQueue.main.async { [weak self] in
                    self?.thumbnail.image = UIImage(data: imageData)
                }
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.thumbnail.image = UIImage(named: "marvel_logo")
                }
            }
        }
    }
}
