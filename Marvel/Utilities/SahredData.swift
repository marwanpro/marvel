//
//  SahredData.swift
//  Marvel
//
//  Created by Marwan Aziz on 21/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation

struct SharedData {
    public static var shared = SharedData()
    var favourites: [Int] = []
}
