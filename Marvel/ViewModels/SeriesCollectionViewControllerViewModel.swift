//
//  SeriesCollectionViewControllerViewModel.swift
//  Marvel
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import Foundation
import DataConnector

public struct SeriesCollectionViewControllerViewModel {

    var seriesData: ObservableValue<DataConnectorSeriesDataModel?> = ObservableValue(nil)
    fileprivate var seriesDataModel: ObservableValue<DataConnectorSeriesDataModel?> = ObservableValue(nil)
    fileprivate var lastSearch: Date?


    init() {
        getData()
    }

    func addToFavourite(seriesId: Int) {
        if !SharedData.shared.favourites.contains(seriesId) {
            SharedData.shared.favourites.append(seriesId)
        }
    }

    mutating func removeFavourite(seriesId: Int) {
        if !SharedData.shared.favourites.isEmpty,
            SharedData.shared.favourites.contains(seriesId) {
            if let index = SharedData.shared.favourites.firstIndex(where: {$0 == seriesId}) {
                SharedData.shared.favourites.remove(at: index)
            }
        }
    }

    func isFavourited(seriesId: Int) -> Bool {
        SharedData.shared.favourites.contains(seriesId)
    }

    func getData() {
        let limit = 99
        var newOffset = 0

        if let offset = seriesDataModel.value?.offset, let total = seriesDataModel.value?.total {
            if offset + limit < total {
                newOffset = offset + limit
            } else if offset < total {
                newOffset = total - offset
            }
        }
        
        DataConnector.shared.getSeries(offset: newOffset, limit: limit) { result in
            switch(result) {
            case .success(let dataModel):
                self.copyNewData(dataModel)
            case .failure(let error):
                print(error)
            }
        }
    }

    fileprivate func copyNewData(_ dataModel: DataConnectorSeriesDataModel) {
        let existingSeriesIds = self.seriesDataModel.value?.series?.filter{ $0.id != nil}.map{$0.id}
        let newSeriesIds = dataModel.series?.filter{ $0.id != nil}.map{$0.id}
        let seriesToKeep = existingSeriesIds?.filter{ !(newSeriesIds?.contains($0) ?? false)}
        let oldSeries = self.seriesDataModel.value?.series?.filter{ seriesToKeep?.contains($0.id) ?? false}

        self.seriesDataModel.value = dataModel
        self.seriesDataModel.value?.series?.append(contentsOf: oldSeries ?? [])
        self.seriesData.value = self.seriesDataModel.value
    }

    fileprivate func searchInternally(text: String) -> DataConnectorSeriesDataModel? {
        if let dataModel = seriesDataModel.value {
            let foundSeries = dataModel.series?.filter { $0.title != nil && $0.title!.starts(with: text)}
            if foundSeries?.isEmpty ?? true {
                return nil
            }
            var copyValue = dataModel
            copyValue.series?.removeAll()
            copyValue.series?.append(contentsOf: foundSeries ?? [])
            return copyValue
        }

        return nil
    }

    mutating func search(_ text: String) {
        if let internalResult = searchInternally(text: text) {
            seriesData.value = internalResult
            print("Internal result")
            return
        }
        let timestamp = Date()
        if let lastDate = lastSearch {
            print("since last date: \(timestamp.timeIntervalSince(lastDate))")
        }
        if lastSearch == nil {
            self.lastSearch = timestamp
            searchSeries(text)
            // At least one second between each search.
        } else if let lastSearch = self.lastSearch, timestamp.timeIntervalSince(lastSearch) >= 1 {
            self.lastSearch = timestamp
            searchSeries(text)
        }
    }

    fileprivate func searchSeries(_ searchText: String) {
        DataConnector.shared.searchSeries(searchText: searchText) { result in
            switch(result) {
            case .success(let dataModel):
                self.seriesData.value = dataModel
            case .failure(let error):
                print(error)
            }
        }
    }
}
