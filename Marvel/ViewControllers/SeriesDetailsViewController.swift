//
//  ViewController.swift
//  Marvel
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import UIKit
import DataConnector

class SeriesDetailsViewController: UIViewController {

    @IBOutlet weak var subViewsContainer: UIView!
    @IBOutlet weak var seriesTitle: UILabel!
    @IBOutlet weak var seriesImage: UIImageView!
    @IBOutlet weak var favoured: UILabel!
    @IBOutlet weak var endYear: UILabel!
    @IBOutlet weak var seriesDescription: UITextView!
    var series: SeriesDetails?



    @IBAction func exitScreen(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    fileprivate func setData() {
        seriesTitle.text = series?.title
        var seriesEndYear = ""
        if let endYear = series?.endYear {
            seriesEndYear = String(endYear)
        }
        endYear.text = "Year: " + seriesEndYear
        seriesDescription.text = series?.description ?? ""
        if let seriesId = series?.id, SharedData.shared.favourites.contains(seriesId) {
            favoured.text = "⭐️"
        } else {
            favoured.text = "✩"
        }
    }

    func loadThumbnailImage(url: String) {
        DispatchQueue.global(qos: .background).async {
            if let imageUrl = URL(string: url), let imageData = try? Data(contentsOf: imageUrl) {
                DispatchQueue.main.async { [weak self] in
                    self?.seriesImage.image = UIImage(data: imageData)
                }
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.seriesImage.image = UIImage(named: "marvel_logo")
                }
            }
        }
    }

    fileprivate func loadSeriesImage() {
        if let thumbnailPath = series?.thumbnail?.path,
                   let imageExtension = series?.thumbnail?.thumbnailExtension {
            let imageType = UIDevice.current.userInterfaceIdiom == .pad ? "/portrait_uncanny." : "/standard_fantastic."
            let imageUrl = thumbnailPath + imageType + imageExtension
            let httpsUrl = imageUrl.replacingOccurrences(of: "http", with: "https")
            print(httpsUrl)
            loadThumbnailImage(url: httpsUrl)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        loadSeriesImage()
        setSupViewsContainerLayout()
    }

    fileprivate func setSupViewsContainerLayout() {
        subViewsContainer.layer.cornerRadius = 15
        subViewsContainer.layer.shadowRadius = 5
        subViewsContainer.layer.shadowOffset = CGSize(width: 5, height: 5)
    }

}

