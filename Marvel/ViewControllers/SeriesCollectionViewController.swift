//
//  SeriesCollectionViewController.swift
//  Marvel
//
//  Created by Marwan Aziz on 20/07/2020.
//  Copyright © 2020 Marwan Aziz. All rights reserved.
//

import UIKit
import DataConnector

private let reuseIdentifier = "SeriesCollectionViewCell"

class SeriesCollectionViewController: UICollectionViewController {

    fileprivate let searchController = UISearchController()
    fileprivate var viewModel = SeriesCollectionViewControllerViewModel()
    fileprivate var series: [SeriesDetails] = []
    
    fileprivate var cellSize: CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIScreen.main.bounds.width / 4 - 15
        } else {
           return UIScreen.main.bounds.width / 2 - 5
        }
    }

    fileprivate func registerCell() {
        self.collectionView.register(UINib(nibName: "SeriesCollectionCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }

    fileprivate func setTheSearchBar() {
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.placeholder = "Search Series"
        self.navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
    }

    fileprivate func bindViewModel() {
        viewModel.seriesData.bind {[weak self] dataModel in
            self?.series = dataModel?.series ?? []
            self?.collectionView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setTheSearchBar()
        bindViewModel()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return series.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? SeriesCollectionViewCell else {
            return UICollectionViewCell()
        }
        let seriesObject = series[indexPath.row]
        cell.seriesId = seriesObject.id ?? 0
        cell.title.text = seriesObject.title
        var endYearString = ""
        if let endYear = seriesObject.endYear {
            endYearString = String(endYear)
        }

        cell.endYear.text = endYearString

        if let thumbnailPath = seriesObject.thumbnail?.path,
                   let imageExtension = seriesObject.thumbnail?.thumbnailExtension {
            let imageUrl = thumbnailPath + "/standard_fantastic." + imageExtension
            let httpsUrl = imageUrl.replacingOccurrences(of: "http", with: "https")
            print(httpsUrl)
            cell.loadThumbnailImage(url: httpsUrl)
        }
        
        cell.starIconTapObserver.bind {[weak self] seriesId in
            if seriesId != 0 {
                if self?.viewModel.isFavourited(seriesId: seriesId) ?? false {
                    self?.viewModel.removeFavourite(seriesId: seriesId)
                    cell.setDisfavoured()
                } else {
                    self?.viewModel.addToFavourite(seriesId: seriesId)
                    cell.setFavoured()
                }
            }
        }

        if viewModel.isFavourited(seriesId: cell.seriesId) {
            cell.setFavoured()
        } else {
            cell.setDisfavoured()
        }
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedSeries = series[indexPath.row]
        self.performSegue(withIdentifier: "toSeriesDetails", sender: selectedSeries)

    }
}

//MARK: Segues
extension SeriesCollectionViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSeriesDetails",
            let selectedSeries = sender as? SeriesDetails,
            let detailController = segue.destination as? SeriesDetailsViewController  {
            detailController.series = selectedSeries
            if UIDevice.current.userInterfaceIdiom == .pad {
                detailController.view.backgroundColor = .clear
                detailController.modalPresentationStyle = .overFullScreen
                let screenBounds = UIScreen.main.bounds
                detailController.preferredContentSize = CGSize(width: screenBounds.width * 0.5, height: screenBounds.height * 0.5)
            }
        }
    }
}

// MARK: ScrollView
extension SeriesCollectionViewController {

    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height

        if maxOffset - offset <= 10.0 {
            self.viewModel.getData()
        }
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension SeriesCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSize, height: cellSize)
    }
}

// MARK: UISearchBarDelegate
extension SeriesCollectionViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.search(searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.getData()
    }
}
